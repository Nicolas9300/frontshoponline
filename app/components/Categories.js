import api from "../helpers/bsale_api.js";

export function Categories() {
  const $div = document.createElement("div");
  $div.classList.add("selects");
  const $selectCategory = document.createElement("select");
  $selectCategory.id = "categories";
  $selectCategory.classList.add("options");

  $selectCategory.innerHTML = `<option value="">Elige una Categoria</option>`;

  $selectCategory.addEventListener("change", (e) => {
    localStorage.setItem("bsCategoryId", e.target.value);
    localStorage.setItem(
      "bsCategory",
      $selectCategory.options[$selectCategory.selectedIndex].text
    );
    api.page = 0;
    location.hash = `#/search?category=${e.target.value}`;
  });

  const $selectOrder = document.createElement("select");
  $selectOrder.id = "order";
  $selectOrder.classList.add("options");

  $selectOrder.innerHTML = `
  <option value="">Ordenar Por:</option>
  <option value="asc">Menor Precio</option>
  <option value="desc">Mayor Precio</option> 
  
  `;

  $selectOrder.addEventListener("change", (e) => {
    api.page = 0;
    localStorage.setItem("bsOrder", e.target.value);
    location.hash = `#/search?order=${
      $selectOrder.options[$selectOrder.selectedIndex].text
    }`;
  });

  $div.appendChild($selectCategory);
  $div.appendChild($selectOrder);

  return $div;
}
