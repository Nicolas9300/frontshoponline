const NAME = "bshop-nico.herokuapp",
  DOMAIN = `https://${NAME}.com`,
  SITE = `${DOMAIN}/api/product/list`,
  CATEGORIES = `${DOMAIN}/api/product/category/list`;

let page = 0;
export default {
  NAME,
  DOMAIN,
  SITE,
  CATEGORIES,
  page,
};
