import { Header } from "./components/Header.js";
import { Loader } from "./components/Loader.js";
import { Main } from "./components/Main.js";
import { Router } from "./components/Router.js";
import { Utils } from "./components/Utils.js";
import { paginacion } from "./helpers/paginacion.js";

export function App() {
  const $root = document.getElementById("root");

  $root.innerHTML = null;
  $root.appendChild(Header());
  $root.appendChild(Utils());
  $root.appendChild(Main());
  $root.appendChild(Loader());

  Router();
  paginacion();
}
